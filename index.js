const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');
const app = express();
app.use(express.json());
const PORT = process.env.PORT || 3000
const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const AdminRoutes = require('./routes/admin')
const SlocRoutes = require('./routes/sloc')
const UsersRoutes = require('./models/users')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
mongoose.connect('mongodb+srv://admin:admin1234@pttproject.fevf6.mongodb.net/pttproject?retryWrites=true&w=majority', { useNewUrlParser: true })
app.use(cors({ origin: true }));
app.use(cors())


app.get('/', (req, res) => {
    res.json({ message: 'Ahoy!' });
});

app.use('/admin', AdminRoutes)
app.use('/users', UsersRoutes)
app.use('/sloc', SlocRoutes)

app.listen(PORT, () => {
    console.log('Application is running on port ' + PORT);
});
