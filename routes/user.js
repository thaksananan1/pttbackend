const router = require('express').Router()
const UsersModel = require('../models/users');


router.get('/get_user', async (req, res) => {
    let user = await UsersModel.find()
    res.status(200).json(user)
})


router.post('/joinparty', async (req, res) => {
    let { firstname, lastname, phone } = req.body
    let payload = { firstname, lastname, phone, SeatOn: '', createAt: new Date() }
    let newuser = new UsersModel(payload)
    await newuser.save()
    res.status(200).json(newuser)
})


module.exports = router