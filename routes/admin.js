const router = require('express').Router()
const AdminModel = require('../models/admin')
const UsersModel = require('../models/users');
const SlocModel = require('../models/sloc')

router.post('/Login', async (req, res) => {
    let { username, password } = req.body;
    let admin = await AdminModel.find();

    // res.json(admin)
    if (admin[0].username == username && admin[0].password == password) res.json({ LoggedIn: true })
    res.json({ LoggedIn: false })
})

router.post('/open_sloc/:amount', async (req, res) => {
    let amount = req.params.amount
    let allsolc = await SlocModel.find()
    for (let i in allsolc) {
        if (i <= amount) {
            await SlocModel.findByIdAndUpdate(allsolc[i]._id, { open_sloc: true })
        } else {
            await SlocModel.findByIdAndUpdate(allsolc[i]._id, { open_sloc: false })
        }
    }
    res.status(200).json({ success: true })
})

router.post('/user_in_sloc', async (req, res) => {
    let { user_id, sloc_id } = req.body
    await SlocModel.findByIdAndUpdate(sloc_id, { user_id, empty: false })
    await UsersModel.findByIdAndUpdate(user_id, { SeatOn: sloc_id })
    res.status(200).json({ success: true })
})

router.post('/leave_user_sloc', async (req, res) => {
    let { user_id } = req.body;
    await UsersModel.findByIdAndUpdate(user_id, { SeatOn: '' })
    await SlocModel.findOneAndUpdate(user_id, { Empty: true, user_id: '' })
})

module.exports = router