const router = require('express').Router()
const SlocModel = require('../models/sloc')

router.get('/create', async (req, res) => {
    let payload = { user_id: "", empty: true, open_sloc: false };
    let newRecord = new SlocModel(payload)
    await newRecord.save()
    res.status(200).json(newRecord)
})

router.get('/all', async (req, res) => {
    let sloc = await SlocModel.find()
    res.status(200).json(sloc)
})

router.get('/open', async (req, res) => {
    let sloc = await SlocModel.find({ open_sloc: true })
    res.status(200).json(sloc)
})

module.exports = router