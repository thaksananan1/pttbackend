const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SeatSloc = new Schema({
    user_id: String,
    empty: Boolean,
    open_sloc: Boolean
})

const SlocModel = mongoose.model('sloc', SeatSloc)

module.exports = SlocModel;