const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Users = new Schema({
    firstname: String,
    lastname: String,
    phone: String,
    createAt: Date,
    SeatOn: String,
})

const UsersModel = mongoose.model('users', Users)

module.exports = UsersModel;